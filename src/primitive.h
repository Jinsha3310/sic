#pragma once
#include <D3D11.h>
#include <DirectXMath.h>
#include <wrl.h>

#include "vector.h"

class Primitive
{
	Microsoft::WRL::ComPtr<ID3D11VertexShader>		vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>		pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>		input_layout;
	Microsoft::WRL::ComPtr<ID3D11Buffer>			buffer;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>	rasterizer_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>	depth_stencil_state;

	Microsoft::WRL::ComPtr<ID3D11Device>			device;
public:
	struct vertex
	{
		VECTOR3F position;
		VECTOR4F color;
	};

	Primitive(ID3D11Device*);
	~Primitive() = default;

	void Rect(ID3D11DeviceContext* device_context, const VECTOR2F& pos, const VECTOR2F& size, const VECTOR2F& center, const float angle, const VECTOR4F& color);
	//void Line(ID3D11DeviceContext* device_context, const VECTOR2F& start, const VECTOR2F& end, const VECTOR4F& color);
	void Circle(ID3D11DeviceContext* device_context, const VECTOR2F& pos, const float radius, const VECTOR4F& color);
};