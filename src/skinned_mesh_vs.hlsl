#include "skinned_mesh.hlsli"

VS_OUT main(float4 postion : POSITION, float4 normal : NORMAL, float2 texcoord : TEXCOORD, float4 bone_weights : WEIGHTS, uint4 bone_indices : BONES)
{
	float3 p = { 0, 0, 0 };
	float3 n = { 0, 0, 0 };
	int i = 0; 
	for (i = 0; i < 4; i++)
	{
		p += (bone_weights[i] * mul(postion, bone_transforms[bone_indices[i]])).xyz;
		n += (bone_weights[i] * mul(float4(normal.xyz, 0), bone_transforms[bone_indices[i]])).xyz;
	}
	
	postion = float4(p, 1.0f);
	normal = float4(n, 0.0f);

	VS_OUT vout;
	vout.position = mul(postion, view_projection);

	normal.w = 0;
	float4 N = normalize(normal);
	float4 L = normalize(-light_direction);


	vout.color = material_color*0.5 + material_color * max(0, dot(L, N)*0.5);
	//vout.color = material_color * max(0, dot(L, N));
	vout.color.a = material_color.a;


	vout.texcoord = texcoord;

	return vout;
}