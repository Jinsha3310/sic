#include "skinned_mesh.h"
#include "resource.h"
#include "fbx_loader.h"

#include <iostream>
#include <fstream>
#include <shlwapi.h>

SkinnedMesh::SkinnedMesh(ID3D11Device* device, const char* filename, const char* extension)
{
	std::unique_ptr<ModelData> model_data;
	model_data = std::make_unique<ModelData>();


	if (PathFileExistsA((std::string(filename) + extension).c_str()))
	{
		std::ifstream ifs;
		ifs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryInputArchive i_archive(ifs);
		i_archive(*model_data);
	}
	else
	{
		FBXLoader::LoadFBX(filename, *model_data);

		std::ofstream ofs;
		ofs.open((std::string(filename) + extension).c_str(), std::ios::binary);
		cereal::BinaryOutputArchive o_archive(ofs);
		o_archive(*model_data);
	}

	std::shared_ptr<ModelResource> model_resource = std::make_shared<ModelResource>(device, std::move(model_data));

	model = std::make_unique<Model>(model_resource);
	model_renderer = std::make_unique<ModelRenderer>(device);
}

SkinnedMesh::~SkinnedMesh()
{

}

void SkinnedMesh::Render(ID3D11DeviceContext* context,
	const DirectX::XMFLOAT4X4& view_projection,
	const VECTOR4F& light,
	const VECTOR4F& color,
	bool            modeling_method)
{
	model_renderer->SetModeling(modeling_method);

	model_renderer->Begin(context, view_projection, light);
	model_renderer->Render(context, *model, color);
	model_renderer->End(context);
}