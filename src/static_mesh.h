#pragma once
#include <d3d11.h>
#include <wrl.h>
#include <vector>
#include <string>

#undef max
#undef min

#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/types/vector.hpp>

#include "vector.h"

#define OBJ_NAME_LENGTH 256


class StaticMesh
{
protected:
	struct Vertex
	{
		VECTOR3F position;
		VECTOR3F normal;
		VECTOR2F texcoord;

		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(position.x, position.y, position.z,
				normal.x, normal.y, normal.z,
				texcoord.x, texcoord.y);
		}
	};

	struct CbScene
	{
		DirectX::XMFLOAT4X4 view_projection;
		VECTOR4F light_direction;
	};
	struct CbMesh
	{
		DirectX::XMFLOAT4X4 world;
	};
	struct CbSubset
	{
		VECTOR4F material_color;
	};

	Microsoft::WRL::ComPtr<ID3D11VertexShader>			vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>			input_layout;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>			pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				vertex_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				index_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				cb_scene;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				cb_mesh;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				cb_subset;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>		wireframe_state;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>		solid_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>		depth_stencil_state;
	D3D11_TEXTURE2D_DESC								texture_desc;
	Microsoft::WRL::ComPtr<ID3D11SamplerState>          sampler_state;


	//OBJファイル用
	unsigned int current_index = 0;
	std::vector<Vertex> vertices;
	std::vector<unsigned int> indices;
	std::vector<VECTOR3F> positions;
	std::vector<VECTOR3F> normals;
	std::vector<VECTOR2F> texcoords;


	struct Material
	{
		unsigned int illum;

		std::wstring newmtl_name;
		VECTOR3F ambient;
		VECTOR3F diffuse;
		VECTOR3F specular;
		float shininess;
		float alpha;
		std::wstring diffuse_map_name;

		Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>    shader_resource_view;


		Material()
		{
			illum = 0;
			newmtl_name = L'\0';
			ambient = VECTOR3F(0.2f, 0.2f, 0.2f);
			diffuse = VECTOR3F(0.8f, 0.8f, 0.8f);
			specular = VECTOR3F(1.0f, 1.0f, 1.0f);
			shininess = 0.0f;
			alpha = 0.0f;

			diffuse_map_name = L'\0';
		}

		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(CEREAL_NVP(illum), CEREAL_NVP(newmtl_name),
				ambient.x, ambient.y, ambient.z,
				diffuse.x, diffuse.y, diffuse.z,
				specular.x, specular.y, specular.z,
				CEREAL_NVP(shininess), CEREAL_NVP(alpha), CEREAL_NVP(diffuse_map_name));
		}
	};
	struct Subset {
		std::wstring usemtl;
		size_t index_start = 0;
		size_t index_count = 0;

		template<class Archive>
		void serialize(Archive& archive)
		{
			archive(usemtl, index_start, index_count);
		}
	};

	std::vector<Subset> subsets;
	std::vector<Material> materials;
public:
	void GetResourcePath(wchar_t(&combinedResourcePath)[256], const wchar_t* referrer_filename, const wchar_t* referent_filename)
	{
		std::wstring s(referrer_filename);
		size_t pos = s.find_last_of(L"/");

		std::wstring path = s.substr(0, pos + 1);
		path += referent_filename;
		wcscpy_s(combinedResourcePath, path.c_str());
	}
	bool LoadOBJFile(ID3D11Device* device, const wchar_t* filename, bool flipping_v_coodinates);
	bool LoadMTLFile(const wchar_t* filename);
	bool CreateBuffer(ID3D11Device* device, Vertex* vertices, int vertex_num, unsigned int* indeces, int index_num);
public:
	void Render(ID3D11DeviceContext* device_context,
		const DirectX::XMFLOAT4X4& world_view_projection,
		const DirectX::XMFLOAT4X4& world,
		const VECTOR4F& light,
		const VECTOR4F& color,
		bool            color_in);

	StaticMesh(ID3D11Device* device, const char* obj_filename, bool flipping_v_coodinates = true, const char* extension = ".bin");
	~StaticMesh() { };

	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(vertices, indices, subsets, materials);
	}
};