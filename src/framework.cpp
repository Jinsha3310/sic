#include "framework.h"
#include "scene_title.h"
#include "scene_game.h"
#include "input.h"
#include <DirectXMath.h>
#include <stdio.h>

std::unique_ptr<Debug> FrameWork::font = {};

bool FrameWork::Initialize()
{
	if (FAILED(InitDevice())) 
	{
		return false;
	}

#ifdef USE_IMGUI
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX11_Init(device.Get(), device_context.Get());

	ImGui::StyleColorsDark();
#endif

	//**************************************************//
	//		DirectX初期化終了	以降描画OBJの初期化		//
	//**************************************************//
	Blender::Initialize(FrameWork::GetDevice(), FrameWork::GetContext());
	Blender::Set(Blender::BLEND_MODE::NONE, FrameWork::GetContext());

	DepthStencilState::Initialize(FrameWork::GetDevice());
	
	//デバッグ文字列の初期化
	font = std::make_unique<Debug>(FrameWork::GetDevice(), L"./DATA/fonts/font0.png", 8192U, SCREEN_HEIGHT);

	current_scene = "TITLE";
	scenes["TITLE"] = std::make_unique<SceneTitle>();
	scenes["GAME"] = std::make_unique<SceneGame>();
	scenes[current_scene]->Initialize(device.Get());


	return true;
}



void FrameWork::Update(float elapsed_time/*Elapsed seconds from last frame*/)
{
	static const char *next_scene = 0;

	input::KeyboardManager::GetInstance().Update();
	next_scene = scenes[current_scene]->Update(elapsed_time);

	if (input::KeyboardManager::GetInstance().RisingState(VK_F1))
	{
		current_scene == "TITLE" ? next_scene = "GAME" : next_scene = "TITLE";
	}

	if (next_scene)
	{
		scenes[current_scene]->Finalize();
		scenes[next_scene]->Initialize(device.Get());
		current_scene = next_scene;
		timer.reset();
	}

	//cube->model->Animate(elapsed_time);

}


void FrameWork::Render(float elapsed_time/*Elapsed seconds from last frame*/)
{
	// Just clear the backbuffer
	float ClearColor[4] = { 0.2f, 0.0f, 0.2f, 1.0f }; //red,green,blue,alpha
	
	// レンダーターゲットビューを通し、バックバッファテクスチャの色を塗りつぶす。
	device_context->ClearRenderTargetView(render_target_view.Get(), ClearColor);
	// 深度ステンシルビューを通し、深度ステンシルテクスチャの深度情報とステンシル情報をクリアする。
	device_context->ClearDepthStencilView(depth_stencil_view.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	// これから描画する情報の書き込む先の設定。
	device_context->OMSetRenderTargets(1, render_target_view.GetAddressOf(), depth_stencil_view.Get());

	//*******************************************************************************************************//
	//			描画開始																					 //
	//*******************************************************************************************************//
	//描画処理時間計算
	benchmark bm;
	bm.begin();

	scenes[current_scene]->Render(device_context.Get());
	
	float bm_timer = bm.end();

	static const int MAX = 100;
	static float arrayTimer[MAX] = {};
	static int pointer = 0;

	arrayTimer[pointer] = bm_timer;
	pointer = (pointer + 1) % MAX;

	float sum = 1000;
	for (auto& t : arrayTimer)
	{
		if (t == 0.0f)	continue;
		if (sum > t)	sum = t;
	}
	
	//Blender::Set(Blender::BLEND_MODE::ADD, FrameWork::GetContext());
	////デバッグ文字列表示
	//font->DrawString(FrameWork::GetContext(),0.f, 0.f, 1.f, 1.f, 1.f, 1.f, 1.f, "BENCH MARK:%f ms", sum * 1000);
	//Blender::Set(Blender::BLEND_MODE::NONE, FrameWork::GetContext());

#ifdef USE_IMGUI

	ImGui::SetNextWindowSize(ImVec2(240.f, 96.f));
	ImGui::Begin("Debug");
	ImGui::Text("SCENE %s", current_scene.c_str());
	ImGui::Text("benchmark:%f ms", sum * 1000);
	ImGui::End();
#endif

	//*******************************************************************************************************//
	//			描画終了																					 //
	//*******************************************************************************************************//
	
	// バックバッファに描画した画を表示する
	//swap_chain->Present(1, 0);

}

HRESULT FrameWork::InitDevice()
{
	HRESULT hr = S_OK;

	//画面サイズ設定
	RECT rc;
	GetClientRect(hwnd, &rc);
	UINT width  = rc.right - rc.left;
	UINT height = rc.bottom - rc.top;

	// デバイス
	// スワップチェーンの生成
	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
	D3D_DRIVER_TYPE driverTypes[] =
	{
		D3D_DRIVER_TYPE_HARDWARE,
		D3D_DRIVER_TYPE_WARP,
		D3D_DRIVER_TYPE_REFERENCE,
	};
	UINT numDriverTypes = ARRAYSIZE(driverTypes);

	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	//スワップチェインを作成する設定
	DXGI_SWAP_CHAIN_DESC swap_chain_desc;
	swap_chain_desc.BufferDesc.Width = width;
	swap_chain_desc.BufferDesc.Height = height;
	swap_chain_desc.BufferDesc.RefreshRate.Numerator = 60;
	swap_chain_desc.BufferDesc.RefreshRate.Denominator = 1;
	swap_chain_desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// 1ピクセルあたりの各色(RGBA)を8bit(0〜255)のテクスチャ(バックバッファ)を作成する。
	swap_chain_desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swap_chain_desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

	swap_chain_desc.SampleDesc.Count = 1;
	swap_chain_desc.SampleDesc.Quality = 0;
	swap_chain_desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swap_chain_desc.BufferCount = 1;		// バックバッファの数
	swap_chain_desc.OutputWindow = hwnd;	// DirectXで描いた画を表示するウインドウ
	swap_chain_desc.Windowed = TRUE;		// ウインドウモードか、フルスクリーンにするか。
	swap_chain_desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swap_chain_desc.Flags = 0; // DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		D3D_DRIVER_TYPE driverType = driverTypes[driverTypeIndex];
		D3D_FEATURE_LEVEL featureLevel;
		hr = D3D11CreateDeviceAndSwapChain(
			NULL,				// どのビデオアダプタを使用するか？既定ならばnullptrで、IDXGIAdapterのアドレスを渡す。
			driverType,			// ドライバのタイプを渡す。D3D_DRIVER_TYPE_HARDWARE 以外は基本的にソフトウェア実装で、特別なことをする場合に用いる。
			NULL,				// 上記をD3D_DRIVER_TYPE_SOFTWAREに設定した際に、その処理を行うDLLのハンドルを渡す。それ以外を指定している際には必ずnullptrを渡す。
			createDeviceFlags,	// 何らかのフラグを指定する。詳しくはD3D11_CREATE_DEVICE列挙型で検索。
			featureLevels,		// ここでD3D_FEATURE_LEVEL列挙型の配列を与える。
			numFeatureLevels,	// 上記の引数で、配列を与えていた場合、要素数を記述する。
			D3D11_SDK_VERSION,	// SDKのバージョン。必ずこの値。
			&swap_chain_desc,	// DXGI_SWAP_CHAIN_DESC構造体のアドレスを設定する。ここで設定されたパラメータでSwapChainが作成される。
			swap_chain.GetAddressOf(),		// 作成成功時、SwapChainのアドレスを格納するアドレス。ここでポインタ経由でSwapChainを操作。。
			device.GetAddressOf(),			// 上記と同様で、Deviceのポインタ変数のアドレスを設定。。。
			&featureLevel,		// 成功したD3D_FEATURE_LEVELを格納するためのD3D_FEATURE_LEVEL列挙型変数のアドレスを設定。
			device_context.GetAddressOf());	// SwapChainとDeviceと同様に、Contextのポインタ変数のアドレスを設定。
		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
		return hr;

	// レンダーターゲットビュー作成
	
	ID3D11Texture2D* pBackBuffer = NULL;
	// SwapChainのバックバッファテクスチャを取得
	// バックバッファテクスチャは色を書き込むテクスチャ
	hr = swap_chain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
	if (FAILED(hr))
		return hr;
	// バックバッファテクスチャの書き込み口となるレンダーターゲットビューを生成
	hr = device->CreateRenderTargetView(pBackBuffer, NULL, render_target_view.GetAddressOf());
	pBackBuffer->Release();
	if (FAILED(hr))
		return hr;


	//深度ステンシルビュー作成

	// 深度ステンシル情報を書き込むためのテクスチャを作成する。
	ID3D11Texture2D *pDepthStencilTexture;
	D3D11_TEXTURE2D_DESC td;
	ZeroMemory(&td, sizeof(td));
	td.Width			  = width;
	td.Height			  = height;
	td.MipLevels		  = 1;
	td.ArraySize		  = 1;
	td.Format			  = DXGI_FORMAT_D24_UNORM_S8_UINT;	// 1ピクセルあたり、深度情報を24ビット・ステンシル情報を8ビットのテクスチャ作成
	td.SampleDesc.Count   = 1;
	td.SampleDesc.Quality = 0;
	td.Usage			  = D3D11_USAGE_DEFAULT;
	td.BindFlags		  = D3D11_BIND_DEPTH_STENCIL;		// 深度ステンシル用のテクスチャ作成
	td.CPUAccessFlags	  = 0;
	td.MiscFlags		  = 0;
	hr = device->CreateTexture2D(&td, NULL, &pDepthStencilTexture);
	if (FAILED(hr))
		return hr;

	D3D11_DEPTH_STENCIL_VIEW_DESC dsDesc;
	ZeroMemory(&dsDesc, sizeof(dsDesc));
	dsDesc.Format			  = td.Format;
	dsDesc.ViewDimension	  = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsDesc.Texture2D.MipSlice = 0;
	// 深度ステンシルテクスチャへの書き込み口になる深度ステンシルビュー作成。
	hr = device->CreateDepthStencilView(pDepthStencilTexture, &dsDesc, depth_stencil_view.GetAddressOf());
	
	pDepthStencilTexture->Release();
	if (FAILED(hr))
		return hr;

	UINT m4xMsaaQuality;
	device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, 4, &m4xMsaaQuality);
	assert(m4xMsaaQuality > 0);

	// ビューポート設定
	// 画面のどの領域に表示するかの設定。
	D3D11_VIEWPORT vp;
	vp.Width = (FLOAT)SCREEN_WIDTH;
	vp.Height = (FLOAT)SCREEN_HEIGHT;
	vp.MinDepth = 0.0f;
	vp.MaxDepth = 1.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;
	device_context->RSSetViewports(1, &vp);
	return S_OK;
}
