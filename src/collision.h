#pragma once

#include "vector.h"


bool isHitSegmentVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D);
VECTOR2F GetHitSegmentPosVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D);
