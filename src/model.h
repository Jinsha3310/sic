#pragma once
#include <memory>
#include <d3d11.h>
#include "model_data.h"

class Model
{
public:
	Model(std::shared_ptr<ModelResource>& resource);
	~Model() {}

	struct Bone
	{
		const char*	name;
		Bone*		parent;
		VECTOR3F	scale;
		VECTOR4F	rotate;
		VECTOR3F	translate;
		FLOAT4X4	local_transform;
		FLOAT4X4	world_transform;
	};


	bool IsPlayAnimation() const { return !end_animation; }
	void PlayAnimation(unsigned int animation_index);
	void Animate(const float elapsed_time);

	void CalculateLocalTransform();
	void CalculateWorldTransform(const DirectX::XMMATRIX& world_transform);

	const std::vector<Bone>& GetBones() const { return bones; }
	const ModelResource* GetModelResource() const { return model_resource.get(); }
private:
	std::shared_ptr<ModelResource>	model_resource;
	std::vector<Bone>				bones;

	
	unsigned int	animation_clip;
	float			current_seconds = 0.0f;
	unsigned int	animation_frame = 0;
	bool			end_animation = false;
};