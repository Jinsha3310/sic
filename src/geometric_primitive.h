#pragma once
#include <d3d11.h>
#include <wrl.h>

#include "vector.h"

class GeometricPrimitive 
{
protected:
	struct Vertex
	{
		VECTOR3F position;
		VECTOR3F normal;
	};

	struct Cbuffer
	{
		DirectX::XMFLOAT4X4 world_view_projection;
		DirectX::XMFLOAT4X4 world;
		VECTOR4F material_color;
		VECTOR4F light_direction;
	};

	Microsoft::WRL::ComPtr<ID3D11VertexShader>			vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>			input_layout;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>			pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				vertex_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				index_buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				constant_buffer;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>		wireframe_state;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>		solid_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>		depth_stencil_state;


	bool CreateBuffer(ID3D11Device* device, Vertex* vertices, int vertex_num, unsigned int* indeces, int index_num);

	GeometricPrimitive() : vertex_shader(nullptr), input_layout(nullptr), pixel_shader(nullptr),
		vertex_buffer(nullptr), index_buffer(nullptr), constant_buffer(nullptr), wireframe_state(nullptr), solid_state(nullptr), depth_stencil_state(nullptr)
	{};
public:
	GeometricPrimitive(ID3D11Device* device);
	virtual ~GeometricPrimitive();

	void Render(ID3D11DeviceContext* device_context,
		const DirectX::XMFLOAT4X4& world_view_projection,
		const DirectX::XMFLOAT4X4& world,
		const VECTOR4F& light,
		const VECTOR4F& color, 
		bool            color_in);
};

class GeometricCube : public GeometricPrimitive
{
public:
	GeometricCube(ID3D11Device* device);
	~GeometricCube() {};
};

class GeometricCylinder : public GeometricPrimitive
{
public:
	GeometricCylinder(ID3D11Device* device, const unsigned int slices, const float radius);
	~GeometricCylinder() {};
};

class GeometricSphere : public GeometricPrimitive
{
public:
	GeometricSphere(ID3D11Device* device, unsigned int slices = 16, unsigned int stacks = 16);
	~GeometricSphere() {};
};

class GeometricCapsule : public GeometricPrimitive
{
public:
	GeometricCapsule(ID3D11Device* device);
	~GeometricCapsule() {};
};