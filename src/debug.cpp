#include "debug.h"
#include "framework.h"

Debug::Debug(ID3D11Device* device, const wchar_t* filename, unsigned int maxInstance, int screen_h) :SpriteBatch(device,filename,maxInstance), posY(0), screenHeight(screen_h)
{
	
}

Debug::~Debug()
{
}

//void Debug::SetString(const char* format, ...)
//{
//	char str[256];
//	vsprintf_s(str, format, (char *)(&format + 1));
//
//	debugQueue.push(str);
//}

void Debug::DrawString(ID3D11DeviceContext* context, float posX, float posY, float r, float g, float b, float scaleX, float scaleY, const char* str,...)
{
	char string[256];
	vsprintf_s(string, str, (char *)(&str + 1));//引数のデータをstring[256]にコピー

	std::string s(string);

	Begin(context);
	

	Textout(s, posX, posY, scaleX, scaleY, r, g, b, 1.f);

	/*while (!debugQueue.empty()) {
		std::string t = debugQueue.front();

		posY += textOut(t, Vector2(0.f, posY), Vector2(scaleX, scaleY), Vector4(r, g, b, a));

		debugQueue.pop();

		if (posY > screenHeight) {
			while (!debugQueue.empty()) {
				debugQueue.pop();
			}
			break;
		}
	}*/

	End(context);
}