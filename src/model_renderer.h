#pragma once
#include <memory>
#include <d3d11.h>
#include "model.h"

class ModelRenderer
{
	bool modeling_method;
public:
	ModelRenderer(ID3D11Device* device);
	~ModelRenderer() {}

	void SetModeling(const bool method) { modeling_method = method; }
	void Begin(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light_direction);
	void Render(ID3D11DeviceContext* context, const Model& model, const VECTOR4F& color);
	void End(ID3D11DeviceContext* context);

private:
	static const int MAX_BONES = 100;

	struct CbScene
	{
		DirectX::XMFLOAT4X4	view_projection;
		VECTOR4F	light_direction;
	};

	struct CbMesh
	{
		//DirectX::XMFLOAT4X4 world;
		DirectX::XMFLOAT4X4	bone_transforms[MAX_BONES];
	};

	struct CbSubset
	{
		VECTOR4F	material_color;
	};


	Microsoft::WRL::ComPtr<ID3D11Buffer>			cb_scene;
	Microsoft::WRL::ComPtr<ID3D11Buffer>			cb_mesh;
	Microsoft::WRL::ComPtr<ID3D11Buffer>			cb_subset;
													
	Microsoft::WRL::ComPtr<ID3D11VertexShader>		vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>		pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>		input_layout;
													
	//Microsoft::WRL::ComPtr<ID3D11BlendState>		blend_state;
	Microsoft::WRL::ComPtr<ID3D11RasterizerState>	rasterizer_state[2];
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>	depth_stencil_state;

	Microsoft::WRL::ComPtr<ID3D11SamplerState>		sampler_state;
	//Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>	dummy_srv;
};
