#pragma once
#include <DirectXMath.h>
#include <D3D11.h>
#include <queue>


#include "vector.h"
#include "resource.h"


struct vertex {
	VECTOR3F position;
	VECTOR4F color;
	VECTOR2F texcoord;
};

class Sprite {
	Microsoft::WRL::ComPtr<ID3D11VertexShader>          vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>           pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>           input_layout;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				instanceBuffer;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState>       rasterizer_state;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>    shader_resource_view;
	D3D11_TEXTURE2D_DESC								texture_desc;
	Microsoft::WRL::ComPtr<ID3D11SamplerState>          sampler_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>     depth_stencil_state;

	std::string psfile_name;
	std::string vsfile_name;
	std::wstring srvfile_name;

public :
	void Render(ID3D11DeviceContext*,//描画(描画準備)
		const VECTOR2F& pos,
		const VECTOR2F& size,
		const VECTOR2F& tex_pos,
		const VECTOR2F& tex_size,
		const VECTOR2F& center,
		const  float angle,
		const  VECTOR4F& colour,
		bool reverse);
	void Render(ID3D11DeviceContext*,
		const float pos_x, const float pos_y, const float size_x, const float size_y, 
		const float tex_pos_x, const float tex_pos_y, const float tex_size_x, const float tex_size_y,
		const  float angle,
		const  float r, const float g, const float b, const float a, bool reverse);

	Sprite(ID3D11Device* , const wchar_t* file_name);
	~Sprite();
};

constexpr unsigned int maxInstance = 256U;

class SpriteBatch
{
	Microsoft::WRL::ComPtr<ID3D11VertexShader>          vertex_shader;
	Microsoft::WRL::ComPtr<ID3D11PixelShader>           pixel_shader;
	Microsoft::WRL::ComPtr<ID3D11InputLayout>           input_layout;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				buffer;
	Microsoft::WRL::ComPtr<ID3D11Buffer>				instance_buffer;

	Microsoft::WRL::ComPtr<ID3D11RasterizerState>       rasterizer_state;

	Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>    shader_resource_view;
	D3D11_TEXTURE2D_DESC								texture_desc;
	Microsoft::WRL::ComPtr<ID3D11SamplerState>          sampler_state;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilState>     depth_stencil_state;

	std::string psfile_name;
	std::string vsfile_name;
	std::wstring srvfile_name;

	unsigned int MAX_INSTANCES;


	struct instance
	{
		DirectX::XMFLOAT4X4 ndcTransform;
		DirectX::XMFLOAT4 texcoordTransform;
		DirectX::XMFLOAT4 color;
	};

	D3D11_VIEWPORT viewport;

	UINT instanceCount = 0;
	instance* instances = nullptr;
public :
	struct vertex {
		DirectX::XMFLOAT3 position;
		DirectX::XMFLOAT2 texcoord;
	};

	SpriteBatch(ID3D11Device*, const wchar_t*, unsigned int maxinstance = maxInstance);
	~SpriteBatch();

	void Begin(ID3D11DeviceContext*);	//前処理(描画前に一度呼ぶ)

		
	void Render(				//描画(描画準備)
		const VECTOR2F& pos,
		const VECTOR2F& scale,
		const VECTOR2F& tex_pos,
		const VECTOR2F& tex_size,
		const VECTOR2F& center,
		const  float angle,
		const  VECTOR4F& colour,
		bool reverse = false);
	void Render(
		const float pos_x, const float pos_y,
		const float size_x, const float size_y,
		const float tex_pos_x, const float tex_pos_y,
		const float tex_size_x, const float tex_size_y,
		const float centerX, const float centerY,
		const  float angle,
		const  float r, const float g, const float b, const float a,
		bool reverse);

	//テキスト描画
	void Textout(
		std::string s,
		const float& posX, const float& posY,
		const float& scaleX, const float& scaleY,
		const float& r, const float& g, const float& b, const float& a);

	void End(ID3D11DeviceContext*);	//後処理(ここで実際に描画される)
};