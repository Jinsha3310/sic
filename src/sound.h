#pragma once

#include <memory>
#include <map>

#include <Audio.h>
//#include "./DirectXTK-master/Inc/Audio.h"
//#pragma comment(lib,"DirectXTK-master/Audio/Bin/Desktop_2015_DXSDK/Win32/Debug/DirectXTKAudioDX.lib")

namespace sound
{
	class Sound
	{
		std::unique_ptr<DirectX::AudioEngine>			audio_engine;
		std::unique_ptr<DirectX::SoundEffect>			sound_effect;
		std::unique_ptr<DirectX::SoundEffectInstance>	sound_effect_instance;
		float											volume;

	public:
		Sound(const wchar_t* filename) : volume(1.0f)
		{
			//CoInitializeEx(nullptr, COINIT_MULTITHREADED);
			
			audio_engine = std::make_unique<DirectX::AudioEngine>(DirectX::AUDIO_ENGINE_FLAGS::AudioEngine_Default);
#ifdef _DEBUG
			audio_engine = std::make_unique<DirectX::AudioEngine>(DirectX::AUDIO_ENGINE_FLAGS::AudioEngine_Debug);
#endif
		};
		~Sound()
		{
			//Release();
		}

		bool LoadSound(const wchar_t* filename, const float volume);
		void Release();
		void Play(bool isloop);
		void Stop();
		void SetVolume(const float volume);
	};


	class SoundManager
	{
		int handle_num;
		std::map<int, std::unique_ptr<Sound>> sound_data;

	public:
		SoundManager() : handle_num(0) {  sound_data.clear(); }
		~SoundManager() { handle_num = 0; sound_data.clear(); }

		int LoadSound(const wchar_t* filename, float volume = 1.0f);
		void CheckStop(int num);
		void PlayData(int num,  bool isloop = false);
		void SetVolume(int num, float volume);

		static SoundManager& GetInstance()
		{
			static SoundManager instance;
			return instance;
		}
	};
}