#include "model_renderer.h"
#include "resource.h"

ModelRenderer::ModelRenderer(ID3D11Device* device) 
{
	HRESULT hr = S_OK;

	modeling_method = true;

	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION",	0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	0,								D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",		0,	DXGI_FORMAT_R32G32B32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",	0,	DXGI_FORMAT_R32G32_FLOAT,		0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "WEIGHTS",	0,	DXGI_FORMAT_R32G32B32A32_FLOAT,	0,	D3D11_APPEND_ALIGNED_ELEMENT,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BONES",		0,	DXGI_FORMAT_R32G32B32A32_UINT,	0,	D3D11_APPEND_ALIGNED_ELEMENT ,	D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	if (!ResourceManager::LoadVertexShaders(device, "./Data/Shaders/skinned_mesh_vs.cso", layout, ARRAYSIZE(layout), vertex_shader.GetAddressOf(), input_layout.GetAddressOf())) {
		OutputDebugStringA("*********************************************\n");
		OutputDebugStringA("LoadVertexShaders error(SkinnedMesh)\n");
		OutputDebugStringA("*********************************************\n");

		assert(!"頂点シェーダ作成失敗(SkinnedMesh)");
	}

	if (!ResourceManager::LoadPixelShaders(device, "./Data/Shaders/skinned_mesh_ps.cso", pixel_shader.GetAddressOf()))
	{
		OutputDebugStringA("*********************************************\n");
		OutputDebugStringA("LoadPixelShaders error(SkinnedMesh)\n");
		OutputDebugStringA("*********************************************\n");

		assert(!"ピクセルシェーダ作成失敗(SkinnedMesh)");
	}

	//ラスタライザステート ソリッド描画
	D3D11_RASTERIZER_DESC rasterizer_desc;
	rasterizer_desc.FillMode = D3D11_FILL_SOLID;
	rasterizer_desc.CullMode = D3D11_CULL_BACK;
	rasterizer_desc.FrontCounterClockwise = true;
	rasterizer_desc.DepthBias = 0;
	rasterizer_desc.DepthBiasClamp = 0.0f;
	rasterizer_desc.SlopeScaledDepthBias = 0.0f;
	rasterizer_desc.DepthClipEnable = true;
	rasterizer_desc.MultisampleEnable = false;
	rasterizer_desc.ScissorEnable = false;
	rasterizer_desc.AntialiasedLineEnable = false;
	hr = device->CreateRasterizerState(&rasterizer_desc, rasterizer_state[0].GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***********************************************\n");
		OutputDebugStringA("CreateRasterizerState error(SkinnedMesh::solid)\n");
		OutputDebugStringA("***********************************************\n");
		assert(!"ラスタライザ作成失敗(SkinnedMesh::solid)");
	}

	rasterizer_desc.FillMode = D3D11_FILL_WIREFRAME;
	rasterizer_desc.AntialiasedLineEnable = true;

	hr = device->CreateRasterizerState(&rasterizer_desc, rasterizer_state[1].GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************************\n");
		OutputDebugStringA("CreateRasterizerState error(SkinnedMesh::wireframe)\n");
		OutputDebugStringA("***************************************************\n");
		assert(!"ラスタライザ作成失敗(SkinnedMesh::wireframe)");
	}
	
	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	depth_stencil_desc.DepthEnable = true;
	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_LESS;
	depth_stencil_desc.StencilEnable = false;
	depth_stencil_desc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	depth_stencil_desc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	hr = device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("******************************************\n");
		OutputDebugStringA("CreateDepthStencilState error(SkinnedMesh)\n");
		OutputDebugStringA("******************************************\n");
		assert(!"深度ステンシル作成失敗(SkinnedMesh)");
	}

	D3D11_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 1.0f;
	samplerDesc.BorderColor[1] = 1.0f;
	samplerDesc.BorderColor[2] = 1.0f;
	samplerDesc.BorderColor[3] = 1.0f;
	samplerDesc.MinLOD = -FLT_MAX;
	samplerDesc.MaxLOD = FLT_MAX;
	hr = device->CreateSamplerState(&samplerDesc, sampler_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateSamplerState error(SkinnedMesh)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"サンプラーステート作成失敗(SkinnedMesh)");
	}

	D3D11_BUFFER_DESC buffer_desc;
	buffer_desc.ByteWidth = sizeof(CbScene);
	buffer_desc.Usage = D3D11_USAGE_DEFAULT;
	buffer_desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	buffer_desc.CPUAccessFlags = 0;
	buffer_desc.MiscFlags = 0;
	buffer_desc.StructureByteStride = 0;
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_scene.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(SkinnedMesh::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(SkinnedMesh)");
	}

	buffer_desc.ByteWidth = sizeof(CbMesh);
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_mesh.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(SkinnedMesh::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(SkinnedMesh)");
	}

	buffer_desc.ByteWidth = sizeof(CbSubset);
	hr = device->CreateBuffer(&buffer_desc, nullptr, cb_subset.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("********************************************************\n");
		OutputDebugStringA("CreateBuffer error(SkinnedMesh::constant_buffer)\n");
		OutputDebugStringA("********************************************************\n");
		assert(!"定数バッファ作成失敗(SkinnedMesh)");
	}
}

void ModelRenderer::Begin(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light_direction)
{
	ID3D11Buffer* constant_buffers[] = {
		cb_scene.Get(),
		cb_mesh.Get(),
		cb_subset.Get(),
	};

	context->VSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);
	context->PSSetConstantBuffers(0, ARRAYSIZE(constant_buffers), constant_buffers);


	context->VSSetShader(vertex_shader.Get(), nullptr, 0);
	context->PSSetShader(pixel_shader.Get(), nullptr, 0);
	context->IASetInputLayout(input_layout.Get());

	if (modeling_method) context->RSSetState(rasterizer_state[0].Get());
	else	             context->RSSetState(rasterizer_state[1].Get());

	context->OMSetDepthStencilState(depth_stencil_state.Get(), 0);
	context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());

	CbScene scene_data;
	scene_data.view_projection = view_projection;
	scene_data.light_direction = light_direction;
	context->UpdateSubresource(cb_scene.Get(), 0, nullptr, &scene_data, 0, 0);
}

void ModelRenderer::Render(ID3D11DeviceContext* context, const Model& model, const VECTOR4F& color)
{
	unsigned int strides = sizeof(ModelData::Vertex);
	unsigned int offsets = 0;

	for (const ModelResource::Mesh& mesh : model.GetModelResource()->GetMeshes())
	{
		CbMesh mesh_data;
		size_t number_of_bones = mesh.bone_indices.size();
		if (number_of_bones > 0)
		{
			for (size_t i = 0; i < number_of_bones; i++)
			{
				//const ModelResource::Bone& bone = model.GetModelResource()->GetBones().at(i);
				const DirectX::XMMATRIX model_w_t = DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_indices.at(i)).world_transform);
				DirectX::XMMATRIX bone_matrix = DirectX::XMLoadFloat4x4(&mesh.inverse_transforms.at(i)) * DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_indices.at(i)).world_transform);
				XMStoreFloat4x4(&mesh_data.bone_transforms[i], bone_matrix);
			}
		}
		else
		{
			XMStoreFloat4x4(&mesh_data.bone_transforms[0], DirectX::XMLoadFloat4x4(&model.GetBones().at(mesh.bone_index).world_transform));
		}

		context->UpdateSubresource(cb_mesh.Get(), 0, nullptr, &mesh_data, 0, 0);
		
		context->IASetVertexBuffers(0, 1, mesh.vertex_buffer.GetAddressOf(), &strides, &offsets);
		context->IASetIndexBuffer(mesh.index_buffer.Get(), DXGI_FORMAT_R32_UINT, 0);
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


		for (const ModelResource::Subset& subset : mesh.subsets)
		{
			CbSubset subset_data;
			subset_data.material_color = VECTOR4F(subset.material->color.x * color.x, subset.material->color.y * color.y, subset.material->color.z * color.z, subset.material->color.w * color.w);

			context->UpdateSubresource(cb_subset.Get(), 0, nullptr, &subset_data, 0, 0);
			
			context->PSSetShaderResources(0, 1, subset.material->shader_resource_view.GetAddressOf());
			context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
			context->DrawIndexed(subset.index_count, subset.index_start, 0);
		}
	}
}

void ModelRenderer::End(ID3D11DeviceContext* context)
{

}