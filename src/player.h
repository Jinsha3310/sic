#pragma once
#include "skinned_mesh.h"
#include "vector.h"

#include <memory>
#include <vector>

class Player
{
	enum ANIMATION_CLIP { CLIP_IDLE, CLIP_WAIT_MOVE, CLIP_ATK, CLIP_FLY };

	ANIMATION_CLIP	animation_clip = CLIP_IDLE;

	bool IsPlayAnimation() const { return substances.at(animation_clip)->model->IsPlayAnimation(); }
	void PlayAnimation(ANIMATION_CLIP animation_index);
	void Animate(const float elapsed_time);
public:
	Player();
	~Player() {};

	std::vector<std::unique_ptr<SkinnedMesh>> substances;

	VECTOR3F scale;
	VECTOR4F rotate;
	VECTOR3F position;

	enum STATES { STATE_IDLE, STATE_ATK, STATE_FLY };
	STATES state = STATE_IDLE;

	void Update(const float elapsed_time);
	void Render(ID3D11DeviceContext* context, const DirectX::XMFLOAT4X4& view_projection, const VECTOR4F& light, const VECTOR4F& color, bool modeling_method)
	{
		substances.at(animation_clip)->Render(context,view_projection,light,color, modeling_method);
	}
};