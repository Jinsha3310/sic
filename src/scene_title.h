#pragma once
#include <memory>

#include "scene.h"
#include "obj2d_data.h"


class SceneTitle : public Scene
{
public:

	bool Initialize(ID3D11Device *device);
	const char* Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context);
	void Finalize();
};