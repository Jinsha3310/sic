#pragma once
#include <d3d11.h>
#include <wrl.h>
#include <vector>

#include "vector.h"
#include "model.h"
#include "model_renderer.h"


class SkinnedMesh
{
	std::unique_ptr<ModelRenderer> model_renderer;
public:
	std::unique_ptr<Model> model;


	SkinnedMesh(ID3D11Device* device, const char* fbx_filename , const char* extension = ".bin");
	~SkinnedMesh();

	void Render(ID3D11DeviceContext* device_context,
		const DirectX::XMFLOAT4X4& world_view_projection,
		const VECTOR4F& light,
		const VECTOR4F& color,
		bool            modeling_method);
};