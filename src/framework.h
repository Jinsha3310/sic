#pragma once

#include <windows.h>
#include <tchar.h>
#include <sstream>
#include <D3D11.h>
#include <memory>
#include <wrl.h>
#include <map>

#include "scene.h"
#include "misc.h"
#include "high_resolution_timer.h"
#include "blender.h"
#include "depth_stencil.h"
#include "debug.h"

#ifdef USE_IMGUI
#include "./imgui/imgui.h"
#include "./imgui/imgui_impl_dx11.h"
#include "./imgui/imgui_impl_win32.h"
#include "./imgui/imgui_internal.h"
extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam);
#endif

const LONG SCREEN_WIDTH = 1280;
const LONG SCREEN_HEIGHT = 720;

class FrameWork
{

	std::map<std::string, std::unique_ptr<Scene>> scenes;

	std::string current_scene;

	const HWND hwnd = NULL;
	Microsoft::WRL::ComPtr<ID3D11Device>			device = NULL;
	Microsoft::WRL::ComPtr<ID3D11DeviceContext>		device_context = NULL;
	Microsoft::WRL::ComPtr<IDXGISwapChain>			swap_chain = NULL;
	Microsoft::WRL::ComPtr<ID3D11RenderTargetView>  render_target_view = NULL;
	Microsoft::WRL::ComPtr<ID3D11DepthStencilView>  depth_stencil_view = NULL;
	
public:
	static std::unique_ptr<Debug> font;
	FrameWork(HWND hwnd) : hwnd(hwnd)
	{

	}
	~FrameWork()
	{
		Blender::Release();
		DepthStencilState::Release();
		ResourceManager::Release();

#ifdef USE_IMGUI
		ImGui_ImplDX11_Shutdown();
		ImGui_ImplWin32_Shutdown();
		ImGui::DestroyContext();
#endif
	}


	static FrameWork&  GetInstance(HWND hwnd = nullptr)
	{
		static FrameWork instance(hwnd);
		return instance;
	}

	inline static ID3D11Device* GetDevice() 
	{
		return FrameWork::GetInstance().device.Get();
	}

	inline static ID3D11DeviceContext* GetContext()
	{
		return FrameWork::GetInstance().device_context.Get();
	}
	
	int Run()
	{
		MSG msg = { 0 };

		if (!Initialize()) return 0;
		timer.reset();

		while (WM_QUIT != msg.message)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				timer.tick();
				calculate_frame_stats();

#ifdef USE_IMGUI
				ImGui_ImplDX11_NewFrame();
				ImGui_ImplWin32_NewFrame();
				ImGui::NewFrame();
#endif

				Update(timer.time_interval());
				Render(timer.time_interval());

#ifdef USE_IMGUI
				ImGui::Render();
				ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
#endif

				// バックバッファに描画した画を表示する
				swap_chain->Present(2, 0);
			}
		}
		return static_cast<int>(msg.wParam);
	}

	LRESULT CALLBACK handle_message(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
	{
#ifdef USE_IMGUI
		if (ImGui_ImplWin32_WndProcHandler(hwnd, msg, wparam, lparam))
			return 1;
#endif

		switch (msg)
		{
		case WM_PAINT:
		{
			PAINTSTRUCT ps;
			HDC hdc;
			hdc = BeginPaint(hwnd, &ps);
			EndPaint(hwnd, &ps);
			break;
		}
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_CREATE:
			break;
		case WM_KEYDOWN:
			if (wparam == VK_ESCAPE) PostMessage(hwnd, WM_CLOSE, 0, 0);
			break;
		case WM_ENTERSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user grabs the resize bars.
			timer.stop();
			break;
		case WM_EXITSIZEMOVE:
			// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
			// Here we reset everything based on the new window dimensions.
			timer.start();
			break;
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
		}
		return 0;
	}

private:
	
	bool Initialize();
	void Update(float elapsed_time/*Elapsed seconds from last frame*/);
	void Render(float elapsed_time/*Elapsed seconds from last frame*/);
	HRESULT InitDevice();

private:
	high_resolution_timer timer;
	void calculate_frame_stats()
	{
		// Code computes the average frames per second, and also the 
		// average time it takes to render one frame.  These stats 
		// are appended to the window caption bar.
		static int frames = 0;
		static float time_tlapsed = 0.0f;

		frames++;

		// Compute averages over one second period.
		if ((timer.time_stamp() - time_tlapsed) >= 1.0f)
		{
			float fps = static_cast<float>(frames); // fps = frameCnt / 1
			float mspf = 1000.0f / fps;
			std::ostringstream outs;
			outs.precision(6);
			outs << "FPS : " << fps << " / " << "Frame Time : " << mspf << " (ms)";
			SetWindowTextA(hwnd, outs.str().c_str());

			// Reset for next average.
			frames = 0;
			time_tlapsed += 1.0f;
		}
	}
};

