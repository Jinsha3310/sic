#include "collision.h"




//線分の当たり判定
bool isHitSegment(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D)
{
	VECTOR2F vec_AB, vec_AC, vec_AD;
	vec_AB = B - A;
	vec_AC = C - A;
	vec_AD = D - A;

	VECTOR2F vec_CA, vec_CB, vec_CD;
	vec_CA = A - C;
	vec_CB = B - C;
	vec_CD = D - C;

	float v1 = CrossVec2(&vec_AB, &vec_AC);
	float v2 = CrossVec2(&vec_AB, &vec_AD);
	float v3 = CrossVec2(&vec_CD, &vec_CA);
	float v4 = CrossVec2(&vec_CD, &vec_CB);

	if (v1 * v2 <= 0.0f && v3 * v4 <= 0.0f)
		return true;

	return false;
}
VECTOR2F GetHitSegmentPosVec2(const VECTOR2F& A, const VECTOR2F& B, const VECTOR2F& C, const VECTOR2F& D)
{
	VECTOR2F vec_AB = B - A;
	float AB_dist = toDistVec2(vec_AB);
	VECTOR2F vec_AC = C - A;
	VECTOR2F vec_CD = D - C;

	float crs_1 = CrossVec2(&vec_AB, &vec_CD);
	float crs_2 = CrossVec2(&vec_AC, &vec_CD);

	float t1 = crs_2 / crs_1;

	vec_AB *= t1;
	return A + vec_AB;
}