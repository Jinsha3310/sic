#include "player.h"
#include "framework.h"
#include "input.h"
#include <assert.h>

//test
Player::Player()
{
	scale = VECTOR3F(0.5f, 0.5f, 0.5f);
	rotate = VECTOR4F(0.0f, 0.0f, 0.0f, 0.0f);
	position = VECTOR3F(0.0f, 0.0f, 0.0f);

	//PlayAnimation(animation_clip);
}

void Player::PlayAnimation(ANIMATION_CLIP animation_index)
{
	//モデルデータが複数あるから
	substances.at(animation_index)->model->PlayAnimation(0);
	//model->PlayAnimation(animation_index);

	animation_clip = animation_index;
}


void Player::Animate(const float elapsed_time)
{
	static ANIMATION_CLIP prev_animation_clip;
	prev_animation_clip = animation_clip;

	switch (state)
	{
	case STATES::STATE_IDLE:
		switch (animation_clip)
		{
		case ANIMATION_CLIP::CLIP_IDLE:
			animation_clip = CLIP_IDLE;

			break;
		}
		break;
	case STATES::STATE_ATK:
		if (animation_clip != ANIMATION_CLIP::CLIP_ATK)
			animation_clip = CLIP_ATK;

		break;
	case STATES::STATE_FLY:
		switch (animation_clip)
		{
		case ANIMATION_CLIP::CLIP_IDLE:
			break;
		}
		break;
	}


	if (animation_clip != prev_animation_clip)
	{
		PlayAnimation(animation_clip);
	}

	if (!IsPlayAnimation())
	{
		PlayAnimation(animation_clip);

		switch (animation_clip)
		{
		case ANIMATION_CLIP::CLIP_IDLE:
			animation_clip = CLIP_IDLE;
			state = STATES::STATE_IDLE;
			break;
		case ANIMATION_CLIP::CLIP_WAIT_MOVE:
			animation_clip = CLIP_WAIT_MOVE;
			state = STATES::STATE_IDLE;
			break;
		case ANIMATION_CLIP::CLIP_ATK:
			animation_clip = CLIP_IDLE;
			state = STATES::STATE_IDLE;
			break;
		case ANIMATION_CLIP::CLIP_FLY:
			animation_clip = CLIP_FLY;
			state = STATES::STATE_FLY;
			break;
		default:
			break;
		}
	}

	substances.at(animation_clip)->model->Animate(elapsed_time);

}

void Player::Update(const float elapsed_time)
{
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(scale.x, scale.y, scale.z);
		R = DirectX::XMMatrixRotationQuaternion(DirectX::XMVectorSet(rotate.x, rotate.y, rotate.z, rotate.w));
		T = DirectX::XMMatrixTranslation(position.x, position.y, position.z);


		W = S * R * T;
	}

	substances.at(animation_clip)->model->CalculateLocalTransform();
	substances.at(animation_clip)->model->CalculateWorldTransform(W);

	if (input::KeyboardManager::GetInstance().RisingState(input::KeyLabel::Z))
	{
		state = STATES::STATE_ATK;
	}

	static float playback_speed = 1.0f;

#ifdef USE_IMGUI
	//ImGui::Begin(u8"player", &use_imgui, ImGuiWindowFlags_MenuBar);
	ImGui::SetNextWindowSize(ImVec2(300.f, 96.f));
	ImGui::Begin("Danbo");
	ImGui::SliderFloat("playback speed", &playback_speed, 0.0f, 2.0f);

	ImGui::End();
#endif

	Animate(elapsed_time*playback_speed);
}