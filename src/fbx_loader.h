#pragma once
#include <fbxsdk.h> 
#include "model_data.h"



class FBXLoader
{
public:
	static bool LoadFBX(const char* filename, ModelData& data);
private:
	static bool CreateModel(const char* dirname, FbxScene* fbx_scene, ModelData& data);

	static void FetchBones(FbxNode* fbx_node, ModelData& data, int parent_node_index);
	static void FetchBone(FbxNode* fbx_node, ModelData& data, int parent_node_index);

	static void FetchMeshes(FbxNode* fbx_node, ModelData& data);
	static void FetchMesh(FbxNode* fbx_node, FbxMesh* fbx_mesh, ModelData& data);

	static void FetchMaterials(const char* dirname, FbxScene* fbx_scene, ModelData& data);
	static void FetchMaterial(const char* dirname, FbxSurfaceMaterial* fbx_surface_material, ModelData& data);


	static void FetchAnimations(FbxScene* fbx_scene, ModelData& data);
	static void FetchAnimationTakes(FbxScene* fbx_scene, ModelData& data);

	static int FindBoneIndex(ModelData& data, const char* name);
	static int FindMaterialIndex(FbxScene* fbx_scene, const FbxSurfaceMaterial* fbx_surface_material);
};