#include "framework.h"
#include "scene_game.h"
#include "fbx_loader.h"


std::unique_ptr<SkinnedMesh> model;
bool SceneGame::Initialize(ID3D11Device *device)
{
	loading_thread = std::make_unique<std::thread>([&](ID3D11Device* device)
	{
		std::lock_guard<std::mutex> lock(loading_mutex);

		if (!player)
		{
			player = std::make_unique<Player>();


			player->substances.emplace_back();
			player->substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_taiki.fbx");
			player->substances.emplace_back();
			player->substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_taiki_idou.fbx");
			player->substances.emplace_back();
			player->substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_atk.fbx");
			player->substances.emplace_back();
			player->substances.back() = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/danbo_fbx/danbo_fly.fbx");
		}

		if (!model)
			model = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/Zombie Walk.fbx");
	}, device);
	



	return true;
}

const char* SceneGame::Update(float elapsed_time)
{
	if (is_now_loading())
	{
		return 0;
	}

	player->Update(elapsed_time);
	
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(0.1f,0.1f,0.1f);
		R = DirectX::XMMatrixRotationQuaternion(DirectX::XMVectorSet(0,0,0,0));
		T = DirectX::XMMatrixTranslation(0,0,0);


		W = S * R * T;
	}

	if (!model->model->IsPlayAnimation())
	{
		model->model->PlayAnimation(1);
	}
	model->model->CalculateLocalTransform();
	model->model->CalculateWorldTransform(W);
	model->model->Animate(elapsed_time);
	return 0;
}

void SceneGame::Render(ID3D11DeviceContext* context)
{
	if (is_now_loading())
	{
		FrameWork::font->DrawString(context, 500, 500, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, "Now Loading ...");
		
		return;
	}

	VECTOR4F color(1.0f, 1.0f, 1.0f, 1.0f);
	light = VECTOR4F(0.0f, 0.0f, -1.0f, 0.0f);

	DirectX::XMMATRIX V;//�r���[�s��  ���[���h�s����t�s��ł����߂���
	{
		DirectX::XMVECTOR pos, target, upward = { 0.0f, 1.0f, 0.0f, 0.0f };
		pos = DirectX::XMVectorSet(0.0f, 50.0f, -200, 0.0f);
		target = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);

		V = DirectX::XMMatrixLookAtLH(pos, target, upward);
	}
	DirectX::XMMATRIX P;//�v���W�F�N�V�����s��
	{
		D3D11_VIEWPORT viewport;
		unsigned int num_viewport = 1;
		context->RSGetViewports(&num_viewport, &viewport);

		//�������e
		P = DirectX::XMMatrixPerspectiveFovLH(30.f * 0.01745f, viewport.Width / viewport.Height, 0.1f, 1000.f);
		//���s���e
		//P = DirectX::XMMatrixOrthographicLH(static_cast<float>(SCREEN_WIDTH), static_cast<float>(SCREEN_HEIGHT), 0.1f, 1000.f);
	}
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1
	);

	DirectX::XMFLOAT4X4 view_projection;

	DirectX::XMStoreFloat4x4(&view_projection, C*V*P);

	player->Render(context, view_projection, light, color, true);
	model->Render(context, view_projection, light, color, true);

	//FrameWork::font->DrawString(context, 500, 500, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, "GAME");
}

void SceneGame::Finalize()
{
	if (loading_thread && loading_thread->joinable())
	{
		loading_thread->join();
	}
}