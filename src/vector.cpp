#include "vector.h"

float toDistVec2(const VECTOR2F& pos) {
	return sqrtf((pos.x*pos.x) + (pos.y*pos.y));
};

VECTOR2F NormalizeVec2(const float std, VECTOR2F& velocity) {//移動力の標準化
	float dist = toDistVec2(velocity);//標準化前の移動量

	if (std < dist) {//移動距離が標準値より大きいなら

		velocity.x *= std / dist;//標準移動量stdと標準化前の移動量distを掛けて標準化してる
		velocity.y *= std / dist;
	}

	return velocity;
};;

VECTOR2F ConvertVec2(const float conv, VECTOR2F& velocity) {//移動量変換
	float dist = toDistVec2(velocity);//変換前の移動量

	velocity.x *= conv / dist;//標準移動量stdと標準化前の移動量distを掛けて標準化してる
	velocity.y *= conv / dist;

	return velocity;
};

float CrossVec2(const VECTOR2F* v1, const VECTOR2F* v2) {//ベクトルの外積
	return v1->x*v2->y - v1->y*v2->x;
};

float DotVec2(const VECTOR2F* v1, const VECTOR2F* v2) {//ベクトルの内積
	return v1->x*v2->x + v1->y*v2->y;
};