#pragma once

#include "scene.h"
#include "player.h"


#include <memory>
#include <thread>
#include <mutex>

class SceneGame : public Scene
{
	std::unique_ptr<std::thread> loading_thread;
	std::mutex loading_mutex;

	std::unique_ptr<Player> player;
	DirectX::XMFLOAT4X4 view_projection;
	VECTOR4F light;
public:
	~SceneGame()
	{
		if (loading_thread && loading_thread->joinable())
		{
			loading_thread->join();
		}
	}

	bool is_now_loading()
	{
		if (loading_thread && loading_mutex.try_lock())
		{
			loading_mutex.unlock();
			return false;
		}
		return true;
	}

	bool Initialize(ID3D11Device *device);
	const char* Update(float elapsed_time);
	void Render(ID3D11DeviceContext* context);
	void Finalize();
};