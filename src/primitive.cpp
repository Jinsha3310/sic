#include "primitive.h"
#include "resource.h"

Primitive::Primitive(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	vertex vertices[130] = { VECTOR3F(0,0,0),VECTOR4F(0,0,0,0) };

	D3D11_BUFFER_DESC bd = {};
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = sizeof(vertices);			// 頂点バッファのサイズ
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// 頂点バッファ
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;	// GPU→読み取りのみ　CPU→書き込みのみ
	bd.MiscFlags = 0;
	bd.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA initData = {};
	initData.pSysMem = vertices;	// 頂点のアドレス
	initData.SysMemPitch = 0;
	initData.SysMemSlicePitch = 0;

	hr = device->CreateBuffer(&bd, &initData, buffer.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************");
		OutputDebugStringA("CreateBuffer error(Primitive)");
		OutputDebugStringA("*****************************");

		assert(!"頂点バッファの作成に失敗(Primitive)");
		return;
	}

	// 入力レイアウトの定義
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT,    0, 0,     D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 4 * 3, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = ARRAYSIZE(layout);

	//	頂点シェーダーの読み込み
	if (!ResourceManager::LoadVertexShaders(device, "./DATA/Shaders/primitive_vs.cso", layout, numElements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf()))
	{
		OutputDebugStringA("***************************************");
		OutputDebugStringA("LoadVertexShaders error(Primitive)");
		OutputDebugStringA("***************************************");

		assert(!"頂点シェーダーの読み込みに失敗(Primitive)");
		return;
	}
	//	ピクセルシェーダーの作成
	if (!ResourceManager::LoadPixelShaders(device, "./Data/Shaders/primitive_ps.cso", pixel_shader.GetAddressOf()))
	{
		OutputDebugStringA("***************************************");
		OutputDebugStringA("LoadPixelShaders error(Primitive)");
		OutputDebugStringA("***************************************");
		assert(!"ピクセルシェーダーの作成に失敗(Primitive)");
		return;
	}

	// ラスタライザステート設定
	D3D11_RASTERIZER_DESC rasterizer_desc;
	rasterizer_desc.FillMode = D3D11_FILL_WIREFRAME;//
	rasterizer_desc.CullMode = D3D11_CULL_NONE;//	カリング
	rasterizer_desc.FrontCounterClockwise = false;
	rasterizer_desc.DepthBias = 0;
	rasterizer_desc.DepthBiasClamp = 0;
	rasterizer_desc.SlopeScaledDepthBias = 0;
	rasterizer_desc.DepthClipEnable = false;
	rasterizer_desc.ScissorEnable = false;
	rasterizer_desc.MultisampleEnable = false;
	rasterizer_desc.AntialiasedLineEnable = false;
	
	hr = device->CreateRasterizerState(&rasterizer_desc, rasterizer_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************");
		OutputDebugStringA("CreateRasterizerState error(Primitive)");
		OutputDebugStringA("*****************************");

		assert(!"ラスタライザステートの作成に失敗(Primitive)");
		return;
	}

	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	depth_stencil_desc.DepthEnable = FALSE;
	depth_stencil_desc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	depth_stencil_desc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.StencilEnable = FALSE;
	depth_stencil_desc.StencilReadMask = 0xFF;
	depth_stencil_desc.StencilWriteMask = 0xFF;
	depth_stencil_desc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depth_stencil_desc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depth_stencil_desc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depth_stencil_desc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depth_stencil_desc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	hr = device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf());
	if (FAILED(hr))
	{
		OutputDebugStringA("*****************************");
		OutputDebugStringA("CreateDepthStencilState error(Primitive)");
		OutputDebugStringA("*****************************");

		assert(!"デプスステンシルステートの作成に失敗(Primitive)");
		return;
	}
}

void Primitive::Rect(ID3D11DeviceContext* device_context, const VECTOR2F& pos, const VECTOR2F& size, const VECTOR2F& center, const float angle, const VECTOR4F& color)
{
	if (size.x * size.y == 0.0f)
		return;

	D3D11_VIEWPORT viewport;
	UINT viewport_num = 1;
	device_context->RSGetViewports(&viewport_num, &viewport);

	vertex vertices[] =
	{
		{ VECTOR3F(-0.0f, +1.0f, 0), color },
		{ VECTOR3F(+1.0f, +1.0f, 0), color },
		{ VECTOR3F(-0.0f, -0.0f, 0), color },
		{ VECTOR3F(+1.0f, -0.0f, 0), color },
	};

	float sn = sinf(angle);
	float cs = cosf(angle);
	for (int i = 0; i < 4; i++)
	{
		vertices[i].position.x *= size.x;
		vertices[i].position.y *= size.y;
		vertices[i].position.x -= center.x;
		vertices[i].position.y -= center.y;

		float rx = vertices[i].position.x;
		float ry = vertices[i].position.y;
		vertices[i].position.x = rx * cs - ry * sn;
		vertices[i].position.y = rx * sn + ry * cs;
		vertices[i].position.x += pos.x;
		vertices[i].position.y += pos.y;

		vertices[i].position.x = vertices[i].position.x * 2 / viewport.Width - 1.0f;
		vertices[i].position.y = 1.0f - vertices[i].position.y * 2 / viewport.Height;
	}

	D3D11_MAPPED_SUBRESOURCE msr;
	device_context->Map(buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
	memcpy(msr.pData, vertices, sizeof(vertices));
	device_context->Unmap(buffer.Get(), 0);

	UINT stride = sizeof(vertex);
	UINT offset = 0;
	device_context->IASetVertexBuffers(0, 1, buffer.GetAddressOf(), &stride, &offset);
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	device_context->IASetInputLayout(input_layout.Get());
	device_context->RSSetState(rasterizer_state.Get());
	device_context->VSSetShader(vertex_shader.Get(), NULL, 0);
	device_context->PSSetShader(pixel_shader.Get(), NULL, 0);
	device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);

	device_context->Draw(4, 0);
}

//void Primitive::Line(ID3D11DeviceContext* device_context, const VECTOR2F& start, const VECTOR2F& end, const VECTOR4F& color)
//{
//	VECTOR2F v1(start.x, start.y);
//	VECTOR2F v2(end.x,   end.y);
//
//	const float w = toDistVec2(v2 - v1);
//	const float h = 1.f;
//	const float cx = w * 0.5f;
//	const float cy = h * 0.5f;
//	const float x = (start.x + end.x) * 0.5f;
//	const float y = (start.y + end.y) * 0.5f;
//	const float angle = atan2(end.y - start.y, end.x - start.x);
//
//	Rect(device_context, VECTOR2F(x, y), VECTOR2F(w, h), VECTOR2F(cx, cy), angle, color);
//}

void Primitive::Circle(ID3D11DeviceContext* device_context, const VECTOR2F& pos, const float radius, const VECTOR4F& color)
{
	if (radius <= 0.0f)
		return;

	D3D11_VIEWPORT viewport;
	UINT viewport_num = 1;
	device_context->RSGetViewports(&viewport_num, &viewport);

	vertex vertices[130] = { VECTOR3F(0,0,0) };
	float arc = DirectX::XM_PI * 2 / 32;
	vertex *p = &vertices[0];
	for (int i = 0; i <= 32; i++)
	{
		p->position.x = pos.x + cosf(-DirectX::XM_PI*0.5f + arc * i) * radius;
		p->position.y = pos.y + sinf(-DirectX::XM_PI*0.5f + arc * i) * radius;
		p->position.x = p->position.x * 2 / viewport.Width - 1.0f;
		p->position.y = 1.0f - p->position.y * 2 / viewport.Height;
		p->color = color;
		p++;

		p->position.x = pos.x; p->position.y = pos.y;
		p->position.x = p->position.x * 2 / viewport.Width - 1.0f;
		p->position.y = 1.0f - p->position.y * 2 / viewport.Height;
		p->color = color;
		p++;
	}

	D3D11_MAPPED_SUBRESOURCE msr;
	device_context->Map(buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &msr);
	memcpy(msr.pData, vertices, sizeof(vertex) * (32 + 1) * 2);
	device_context->Unmap(buffer.Get(), 0);

	UINT stride = sizeof(vertex);
	UINT offset = 0;
	device_context->IASetVertexBuffers(0, 1, buffer.GetAddressOf(), &stride, &offset);
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	device_context->IASetInputLayout(input_layout.Get());
	device_context->RSSetState(rasterizer_state.Get());
	device_context->VSSetShader(vertex_shader.Get(), NULL, 0);
	device_context->PSSetShader(pixel_shader.Get(), NULL, 0);
	device_context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);

	device_context->Draw((32 + 1) * 2 - 1, 0);
}