#include "scene_title.h"
#include "static_mesh.h"
#include "skinned_mesh.h"
#include <memory>
#include "input.h"

std::unique_ptr<StaticMesh> inc;
std::unique_ptr<SkinnedMesh> mesh;
bool SceneTitle::Initialize(ID3D11Device *device)
{
	//inc = std::make_unique<StaticMesh>(device, "./DATA/OBJ/Mr.Incredible/Mr.Incredible.obj");
	mesh = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/2set_anim.fbx");
	//mesh = std::make_unique<SkinnedMesh>(device, "./DATA/FBX/Zombie Walk.fbx");

	
	mesh->model->PlayAnimation(0);
	return true;
}

static float flont = 0;
static float side = 0;


const char* SceneTitle::Update(float elapsed_time)
{
	DirectX::XMMATRIX W;
	{
		DirectX::XMMATRIX S, R, T;
		S = DirectX::XMMatrixScaling(1.0f, 1.0f, 1.0f);
		R = DirectX::XMMatrixRotationRollPitchYaw(0.0f* 0.01745f, 0.0f * 0.01745f, 0.0f * 0.01745f);
		//RightHand = DirectX::XMMatrixRotationRollPitchYaw(-90.0f * 0.01745f, 0.0f, 0.0f);
		T = DirectX::XMMatrixTranslation(0.0f, 0.0f, 0.0f);


		W = S * R * T;
	}

	mesh->model->CalculateLocalTransform();
	mesh->model->CalculateWorldTransform(W);
	mesh->model->Animate(elapsed_time);

	if (!mesh->model->IsPlayAnimation())
	{
		mesh->model->PlayAnimation(0);
	}

	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::W))
	{
		flont += 2.f;
	}
	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::S))
	{
		flont -= 2.f;
	}
	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::D))
	{
		side += 2.f;
	}
	if (input::KeyboardManager::GetInstance().PressedState(input::KeyLabel::A))
	{
		side -= 2.f;
	}
	return 0;
}

void SceneTitle::Render(ID3D11DeviceContext* context)
{
	DirectX::XMMATRIX V;//�r���[�s��  ���[���h�s����t�s��ł����߂���
	{
		DirectX::XMVECTOR pos, target, upward = { 0.0f, 1.0f, 0.0f, 0.0f };
		pos = DirectX::XMVectorSet(0.0f+ side, 10.0f, -25.f + flont, 0.0f);
		target = DirectX::XMVectorSet(0.0f, 5.0f, 0.0f, 0.0f);

		V = DirectX::XMMatrixLookAtLH(pos, target, upward);
	}
	DirectX::XMMATRIX P;//�v���W�F�N�V�����s��
	{
		D3D11_VIEWPORT viewport;
		unsigned int num_viewport = 1;
		context->RSGetViewports(&num_viewport, &viewport);

		//�������e
		P = DirectX::XMMatrixPerspectiveFovLH(30.f * 0.01745f, viewport.Width / viewport.Height, 0.1f, 1000.f);
		//���s���e
		//P = DirectX::XMMatrixOrthographicLH(static_cast<float>(SCREEN_WIDTH), static_cast<float>(SCREEN_HEIGHT), 0.1f, 1000.f);
	}
	DirectX::XMMATRIX C = DirectX::XMMatrixSet(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, -1, 0,
		0, 0, 0, 1
	);

	DirectX::XMFLOAT4X4 view_projection;

	DirectX::XMStoreFloat4x4(&view_projection, C*V*P);

	//inc->Render(context, view_projection, world, VECTOR4F(0, 0, -1, 0), VECTOR4F(1, 1, 1, 1), true);
	mesh->Render(context, view_projection, VECTOR4F(1, 1, -1, 0), VECTOR4F(1, 1, 1, 1), true);
	//FrameWork::font->DrawString(context, 500, 500, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, "TITLE");
}

void SceneTitle::Finalize()
{
	//ResourceManager::Release();
}