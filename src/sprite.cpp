#include "sprite.h"
#include "framework.h"
#include "resource.h"

#include <stdio.h>
#include <assert.h>



vertex vertices[] = {
	{ VECTOR3F(-0.01f, +0.01f, 0), VECTOR4F(1, 1, 1, 1) },
	{ VECTOR3F(+0.01f, +0.01f, 0), VECTOR4F(1, 0, 0, 1) },
	{ VECTOR3F(-0.01f, -0.01f, 0), VECTOR4F(0, 1, 0, 1) },
	{ VECTOR3F(+0.01f, -0.01f, 0), VECTOR4F(0, 0, 1, 1) },
};

Sprite::Sprite(ID3D11Device* device, const wchar_t* file_name) 
{	

	// ポリゴンを描画するにはGPUに頂点データやシェーダーなどのデータを渡す必要がある。
	// GPUにデータを渡すにはID3D11***のオブジェクトを介してデータを渡す。

	//https://docs.microsoft.com/ja-jp/windows/desktop/api/d3d11/ns-d3d11-d3d11_buffer_desc
	//頂点バッファ作成 点の座標決め
	D3D11_BUFFER_DESC bd;
	//D3D11_USAGE_DYNAMIC  GPU (読み取りのみ) と CPU (書き込みのみ) によるアクセスを可能にする
	bd.Usage		  = D3D11_USAGE_DYNAMIC;		//この頂点バッファがGPUやCPUからどのように使われるか（アクセスされるか）をフラグ指定
	bd.ByteWidth	  = sizeof(vertices);			//作成するバッファのサイズを指定
	bd.BindFlags	  = D3D11_BIND_VERTEX_BUFFER;	//このバッファがどういう種類のバッファであるかを指定
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;		//バッファに対するCPUのアクセス権限を指定
	bd.MiscFlags	  = 0;							//option 0でいいかな。
	bd.StructureByteStride = 0;

	D3D11_SUBRESOURCE_DATA subresource_data;
	subresource_data.pSysMem		  = vertices;				//リソースとなるメモリブロックへのポインタを渡す
	subresource_data.SysMemPitch      = 0;						//リソースがテクスチャの場合にだけ使われる値
	subresource_data.SysMemSlicePitch = 0;						//デプスレベルの大きさを与える
	// 頂点バッファオブジェクトの作成。
	if (FAILED(device->CreateBuffer(&bd, &subresource_data, buffer.GetAddressOf())))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateBuffer error(Sprite)\n");
		OutputDebugStringA("***************************************\n");
		assert("CreateBuffer error(Sprite)");
	}


	//頂点レイアウト作成
	D3D11_INPUT_ELEMENT_DESC vertex_desc[] = {
		{ "POSITION",0,DXGI_FORMAT_R32G32B32_FLOAT,   0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },//POSITIONタグがhlslで使える
		{ "COLOR"   ,0,DXGI_FORMAT_R32G32B32A32_FLOAT,0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",0,DXGI_FORMAT_R32G32_FLOAT,      0,D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT num_elements = ARRAYSIZE(vertex_desc);//上のサイズ分

	char vs_name[128] = "./DATA/Shaders/sprite_vs.cso";
	this->vsfile_name = vs_name;
	if (!ResourceManager::LoadVertexShaders(device, vs_name, vertex_desc, num_elements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf()))
	{
		OutputDebugStringA("***************************************");
		OutputDebugStringA("LoadVertexShaders error(Sprite)");
		OutputDebugStringA("***************************************");

		assert(!"頂点シェーダ作成失敗(Sprite)");
		return;
	}

	char ps_name[128] = "./DATA/Shaders/sprite_ps.cso";
	this->psfile_name = ps_name;
	if (!ResourceManager::LoadPixelShaders(device, ps_name, pixel_shader.GetAddressOf())) {
		OutputDebugStringA("***************************************");
		OutputDebugStringA("LoadPixelShaders error(Sprite)");
		OutputDebugStringA("***************************************");

		assert(!"ピクセルシェーダ作成失敗(Sprite)");
		return;
	}

	this->srvfile_name = file_name;
	if (!ResourceManager::LoadShaderResourceViews(device, file_name, shader_resource_view.GetAddressOf(), &texture_desc)) {
		OutputDebugStringA("***************************************");
		OutputDebugStringA("LoadShaderResourceView error(Sprite)");
		OutputDebugStringA("***************************************");
		assert(!"テクスチャ読み込み失敗(Sprite)");
		return;
	}

	//https://docs.microsoft.com/en-us/windows/desktop/api/d3d11/ns-d3d11-d3d11_sampler_desc
	//サンプラーステート
	D3D11_SAMPLER_DESC samplar_desc;
	samplar_desc.Filter			= D3D11_FILTER_MIN_MAG_MIP_LINEAR;  // テクスチャをサンプリングするときに使用する方法
	samplar_desc.AddressU		= D3D11_TEXTURE_ADDRESS_MIRROR;   // 0から1の範囲外のauテクスチャ座標を解決するために使用するメソッド
	samplar_desc.AddressV		= D3D11_TEXTURE_ADDRESS_MIRROR;   // 0から1の範囲外のテクスチャ座標を解決するために使用するメソッド
	samplar_desc.AddressW		= D3D11_TEXTURE_ADDRESS_MIRROR;   // 0から1の範囲外のWテクスチャ座標を解決するために使用するメソッド
	samplar_desc.MipLODBias		= 0;							// 計算されたミップマップレベルからオフセット。
	samplar_desc.MaxAnisotropy	= 1;							// D3D11_FILTER_ANISOTROPICまたはD3D11_FILTER_COMPARISON_ANISOTROPICがファイルで指定されてる場合に使用されるクランプ値。有効値は1〜16です
	samplar_desc.ComparisonFunc = D3D11_COMPARISON_NEVER;	// サンプリングデータの既存のサンプリングデータと比較する関数。
	samplar_desc.BorderColor[0] = 1.0f;						// AddressU、AddressV、またはAddressWにD3D11_TEXTURE_ADDRESS_BORDERが指定されている場合に使用する枠の色。範囲は0.0以上1.0以下でなければなりません。
	samplar_desc.BorderColor[1] = 1.0f;	
	samplar_desc.BorderColor[2] = 1.0f;	
	samplar_desc.BorderColor[3] = 1.0f;	
	samplar_desc.MinLOD = -FLT_MAX;							// アクセスを制限するミップマップ範囲の下限。0は最大かつ最も詳細なミップマップレベルで、それより高いレベルは詳細度が低くなります。
	samplar_desc.MaxLOD = FLT_MAX;							// アクセスを制限するミップマップ範囲の上限。この値はMinLOD以上でなければならない。

	if (FAILED(device->CreateSamplerState(&samplar_desc, sampler_state.GetAddressOf())))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateSamplerState error(Sprite)\n");
		OutputDebugStringA("***************************************\n");
		assert("CreateSamplerState error(Sprite)");
	}

	//https://docs.microsoft.com/en-us/windows/desktop/api/d3d11/ns-d3d11-d3d11_rasterizer_desc
	//ラスタライザステート
	D3D11_RASTERIZER_DESC rasterizer_desc;
	rasterizer_desc.FillMode				= D3D11_FILL_SOLID;	// レンダリング時に使用する塗りつぶしモードを決定します
	rasterizer_desc.CullMode				= D3D11_CULL_NONE;	// 指定された方向を向いている三角形が描かれていないことを示します
	rasterizer_desc.FrontCounterClockwise	= true;				// 三角形が正面を向いているか背面を向いているかを決定します。
	rasterizer_desc.DepthBias				= 0;				// 特定のピクセルに追加された深度値。
	rasterizer_desc.DepthBiasClamp			= 0.0f;				// ピクセルの最大深度バイアス
	rasterizer_desc.SlopeScaledDepthBias	= 0.0f;				// 与えられたピクセルの傾きのスカラー
	rasterizer_desc.DepthClipEnable			= false;			// 距離に基づいてクリッピングを有効にします
	rasterizer_desc.ScissorEnable			= false;			// シザー長方形カリングを有効にします
	rasterizer_desc.MultisampleEnable		= false;			// マルチサンプルアンチエイリアシング（MSAA）レンダーターゲットで四辺形またはアルファ線のアンチエイリアシングアルゴリズムを使用するかどうかを指定します
	rasterizer_desc.AntialiasedLineEnable	= false;			// ラインアンチエイリアシングを有効にするかどうかを指定します



	if (FAILED(device->CreateRasterizerState(&rasterizer_desc, rasterizer_state.GetAddressOf())))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateRasterizerState error(Sprite)\n");
		OutputDebugStringA("***************************************\n");
		assert("CreateRasterizerState error(Sprite)");
	}

	D3D11_DEPTH_STENCIL_DESC depth_stencil_desc;
	depth_stencil_desc.DepthEnable			= false;							// 詳細テストを有効に
	depth_stencil_desc.DepthWriteMask		= D3D11_DEPTH_WRITE_MASK_ALL;		// 深度データによって変更できる深度ステンシルバッファの部分を識別
	depth_stencil_desc.DepthFunc			= D3D11_COMPARISON_ALWAYS;			// 深度データを既存の深度データと比較する機能
	depth_stencil_desc.StencilEnable		= false;							// ステンシルテストを有効
	depth_stencil_desc.StencilReadMask		= D3D11_DEFAULT_STENCIL_READ_MASK;	// ステンシルデータを読み取るための深度ステンシルバッファの一部を指定
	depth_stencil_desc.StencilWriteMask		= D3D11_DEFAULT_STENCIL_WRITE_MASK;	// ステンシルデータを書き込むための深度ステンシルバッファの一部を識別
	// FrontFace サーフェス法線がカメラの方を向いているピクセルについて、深度テストとステンシルテストの結果を使用する方法を特定
	depth_stencil_desc.FrontFace.StencilFunc		= D3D11_COMPARISON_ALWAYS;			
	depth_stencil_desc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;	// 
	depth_stencil_desc.FrontFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;	// 
	depth_stencil_desc.FrontFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;	// 
	// BackFace  サーフェス法線がカメラと反対方向を向いているピクセルについて、深度テストとステンシルテストの結果を使用する方法を特定
	depth_stencil_desc.BackFace.StencilFunc			= D3D11_COMPARISON_ALWAYS;	
	depth_stencil_desc.BackFace.StencilDepthFailOp	= D3D11_STENCIL_OP_KEEP;	// 
	depth_stencil_desc.BackFace.StencilFailOp		= D3D11_STENCIL_OP_KEEP;	// 
	depth_stencil_desc.BackFace.StencilPassOp		= D3D11_STENCIL_OP_KEEP;	// 

	if (FAILED(device->CreateDepthStencilState(&depth_stencil_desc, depth_stencil_state.GetAddressOf())))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateDepthStencilState error(Sprite)\n");
		OutputDebugStringA("***************************************\n");
		assert("CreateDepthStencilState error(Sprite)");
	}
}

Sprite::~Sprite()
{
	
}

void Sprite::Render(ID3D11DeviceContext*device_context,	//描画(描画準備)
	const VECTOR2F& pos,
	const VECTOR2F& size,
	const VECTOR2F& tex_pos,
	const VECTOR2F& tex_size,
	const VECTOR2F& center,
	const  float angle,
	const  VECTOR4F& colour,
	bool reverse)
{
	Render(device_context, pos.x, pos.y, size.x, size.y, tex_pos.x, tex_pos.y, tex_size.x, tex_size.y,  angle, colour.x, colour.y, colour.z, colour.w, reverse);
}

void Sprite::Render(ID3D11DeviceContext*device_context, float pos_x, float pos_y, float size_x, float size_y, float tex_pos_x, float tex_pos_y, float tex_size_x, float tex_size_y, float angle, float r, float g, float b, float a, bool reverse)
{
	//DepthStencilState::Set(DepthStencilState::DEPTH_STENCIL_STATE::NONE, FrameWork::GetContext());


	vertex vertice[] = {
		{ VECTOR3F(pos_x         , pos_y         , 0),VECTOR4F(r,g,b,a),VECTOR2F(tex_pos_x             , tex_pos_y)				},
		{ VECTOR3F(pos_x + size_x, pos_y         , 0),VECTOR4F(r,g,b,a),VECTOR2F(tex_pos_x + tex_size_x, tex_pos_y)				},
		{ VECTOR3F(pos_x         , pos_y + size_y, 0),VECTOR4F(r,g,b,a),VECTOR2F(tex_pos_x             , tex_pos_y + tex_size_y) },
		{ VECTOR3F(pos_x + size_x, pos_y + size_y, 0),VECTOR4F(r,g,b,a),VECTOR2F(tex_pos_x + tex_size_x, tex_pos_y + tex_size_y) },
	};
	

	//	幅の拡大
	//for (int i = 0; i < 4; i++) {
	//	vertice[i].position.x *= size.x;
	//	vertice[i].position.y *= size.y;
	//}


	//	回転軸の変更（矩形中央へ）
	for (int i = 0; i < 4; i++) {
		vertice[i].position.x -= (pos_x + size_x / 2.f);
		vertice[i].position.y -= (pos_y + size_y / 2.f);

	}

	//	angle度の回転
	float degree = angle * 0.01745f;
	float sn = sinf(degree);
	float cs = cosf(degree);
	for (int i = 0; i < 4; i++) {
		float bx = vertice[i].position.x;
		float by = vertice[i].position.y;
		vertice[i].position.x = bx * cs - by * sn;
		vertice[i].position.y = bx * sn + by * cs;

	}

	//	回転軸の変更（元の位置へ）
	for (int i = 0; i < 4; i++) {
		vertice[i].position.x += size_x / 2.0f;
		vertice[i].position.y += size_y / 2.0f;
	}

	//	(dx,dy)の平行移動
	for (int i = 0; i < 4; i++) {
		vertice[i].position.x += pos_x;
		vertice[i].position.y += pos_y;
	}

	vertex verticesNDC[4];
	float tex_mirror = 0.f;
	for (int i = 0; i < 4; i++)
	{
		//	NDC変換
		verticesNDC[i].position.x = (vertice[i].position.x / (float)SCREEN_WIDTH)  * 2.0f - 1.0f;
		verticesNDC[i].position.y = -(vertice[i].position.y / (float)SCREEN_HEIGHT) * 2.0f + 1.0f;
		verticesNDC[i].position.z = 0.0f;
		verticesNDC[i].color = vertice[i].color;


		if (reverse)
			tex_mirror = 1.0f;
		//	UV座標正規化
		verticesNDC[i].texcoord.x = vertice[i].texcoord.x / texture_desc.Width + tex_mirror;
		verticesNDC[i].texcoord.y = vertice[i].texcoord.y / texture_desc.Height;
	}

	D3D11_MAPPED_SUBRESOURCE mapped_subresouce;
	if (FAILED(device_context->Map(buffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mapped_subresouce))) {
		assert("map error(Sprite)");
	}
	memcpy(mapped_subresouce.pData, verticesNDC, sizeof(verticesNDC));
	device_context->Unmap(buffer.Get(), 0);
	

	UINT strides = sizeof(vertex);
	UINT offsets = 0;

	device_context->IASetVertexBuffers(0, 1, buffer.GetAddressOf(), &strides, &offsets);
	device_context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	device_context->VSSetShader(vertex_shader.Get(), NULL, 0);
	device_context->IASetInputLayout(input_layout.Get());
	device_context->RSSetState(rasterizer_state.Get());
	device_context->PSSetShader(pixel_shader.Get(), NULL, 0);
	device_context->PSSetShaderResources(0,1,shader_resource_view.GetAddressOf());
	device_context->PSSetSamplers(0,1,sampler_state.GetAddressOf());
	device_context->OMSetDepthStencilState(depth_stencil_state.Get(),1);


	device_context->Draw(4, 0);
}


//***************************************************************
SpriteBatch::SpriteBatch(ID3D11Device* device, const wchar_t* filename, unsigned int maxInstance) : vertex_shader(nullptr), pixel_shader(nullptr),
input_layout(nullptr), buffer(nullptr), rasterizer_state(nullptr), sampler_state(nullptr), depth_stencil_state(nullptr),
instance_buffer(nullptr), instanceCount(0), instances(nullptr), MAX_INSTANCES(maxInstance)
{
	HRESULT hr = S_OK;

	//頂点バッファの作成
	vertex vertices[] = {
		{ DirectX::XMFLOAT3(0, 0, 0), DirectX::XMFLOAT2(0, 0) },
		{ DirectX::XMFLOAT3(1, 0, 0), DirectX::XMFLOAT2(1, 0) },
		{ DirectX::XMFLOAT3(0, 1, 0), DirectX::XMFLOAT2(0, 1) },
		{ DirectX::XMFLOAT3(1, 1, 0), DirectX::XMFLOAT2(1, 1) },
	};

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = sizeof(vertices);
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	bufferDesc.MiscFlags = 0;
	bufferDesc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA subResourceData = {};
	subResourceData.pSysMem = vertices;
	subResourceData.SysMemPitch = 0;
	subResourceData.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&bufferDesc, &subResourceData, buffer.GetAddressOf());
	if (FAILED(hr)) 
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateBuffer error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"頂点バッファ作成失敗(SpriteBatch)");
	}

	

	//頂点レイアウト作成
	D3D11_INPUT_ELEMENT_DESC layout[] = {
		{ "POSITION",           0, DXGI_FORMAT_R32G32B32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,   0 },
		{ "TEXCOORD",           0, DXGI_FORMAT_R32G32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA,   0 },
		{ "NDC_TRANSFORM",      0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "NDC_TRANSFORM",      3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "TEXCOORD_TRANSFORM", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
		{ "COLOR",              0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_INSTANCE_DATA, 1 },
	};
	UINT num_elements = ARRAYSIZE(layout);

	const char* vs_name = "./DATA/Shaders/sprite_batch_vs.cso";
	this->vsfile_name = vs_name;
	if (!ResourceManager::LoadVertexShaders(device, vs_name, layout, num_elements, vertex_shader.GetAddressOf(), input_layout.GetAddressOf()))
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("LoadVertexShaders error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"頂点シェーダ作成失敗(SpriteBatch)");
		return;
	}

	const char* ps_name = "./DATA/Shaders/sprite_batch_ps.cso";
	this->psfile_name = ps_name;
	if (!ResourceManager::LoadPixelShaders(device, ps_name, pixel_shader.GetAddressOf())) {
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("LoadPixelShaders error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"ピクセルシェーダ作成失敗(SpriteBatch)");
		return;
	}

	this->srvfile_name = filename;
	if (!ResourceManager::LoadShaderResourceViews(device, filename, shader_resource_view.GetAddressOf(), &texture_desc)) {
		OutputDebugStringA("*****************************************\n");
		OutputDebugStringA("LoadShaderResourceView error(SpriteBatch)\n");
		OutputDebugStringA("******************************************n");
		assert(!"テクスチャ読み込み失敗(SpriteBatch)");
	}

	//インスタンスバッファの作成
	instance* inst = new instance[MAX_INSTANCES];
	D3D11_BUFFER_DESC inst_buffer_desc = {};
	inst_buffer_desc.ByteWidth = sizeof(instance) * MAX_INSTANCES;
	inst_buffer_desc.Usage = D3D11_USAGE_DYNAMIC;
	inst_buffer_desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	inst_buffer_desc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	inst_buffer_desc.MiscFlags = 0;
	inst_buffer_desc.StructureByteStride = 0;
	D3D11_SUBRESOURCE_DATA inst_subresource_desc = {};
	inst_subresource_desc.pSysMem = inst;
	inst_subresource_desc.SysMemPitch = 0;
	inst_subresource_desc.SysMemSlicePitch = 0;
	hr = device->CreateBuffer(&inst_buffer_desc, &inst_subresource_desc, instance_buffer.GetAddressOf());
	delete[] inst;
	if (FAILED(hr)) 
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateBuffer error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"インスタンスバッファ作成失敗(SpriteBatch)");
		return;
	}

	D3D11_RASTERIZER_DESC rasterizerDesc;
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;
	rasterizerDesc.CullMode = D3D11_CULL_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0;
	rasterizerDesc.SlopeScaledDepthBias = 0;
	rasterizerDesc.DepthClipEnable = FALSE;
	rasterizerDesc.ScissorEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	hr = device->CreateRasterizerState(&rasterizerDesc, rasterizer_state.GetAddressOf());
	if (FAILED(hr)) 
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateRasterizerState error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"ラスタライザステート作成失敗(SpriteBatch)");
		return;
	}

	//サンプラーステート
	//どのようにテクスチャの色を取り出すか設定する
	D3D11_SAMPLER_DESC samplerDesc = {};
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_MIRROR;
	samplerDesc.MipLODBias = 0;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	for (int i = 0; i < 4; i++) {
		samplerDesc.BorderColor[i] = 0;
	}
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	hr = device->CreateSamplerState(&samplerDesc, sampler_state.GetAddressOf());
	if (FAILED(hr)) 
	{
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateSamplerState error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");

		assert(!"サンプラーステート作成失敗(SpriteBatch)");
		return;
	}


	D3D11_DEPTH_STENCIL_DESC dsDesc = {};
	dsDesc.DepthEnable = false;
	dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.StencilEnable = false;
	dsDesc.StencilReadMask = 0xFF;
	dsDesc.StencilWriteMask = 0xFF;
	dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	hr = device->CreateDepthStencilState(&dsDesc, depth_stencil_state.GetAddressOf());
	if (FAILED(hr)) {
		OutputDebugStringA("***************************************\n");
		OutputDebugStringA("CreateDepthStencilState error(SpriteBatch)\n");
		OutputDebugStringA("***************************************\n");
		assert(!"深度ステンシルステート作成失敗(SpriteBatch)");
		return;
	}
}

SpriteBatch::~SpriteBatch()
{
	if (instances)
	{
		instances = nullptr;
	}
}

void SpriteBatch::Begin(ID3D11DeviceContext* context)
{
	HRESULT hr = S_OK;

	UINT strides[2] = { sizeof(vertex), sizeof(instance) };
	UINT offsets[2] = { 0, 0 };
	ID3D11Buffer *vbs[2] = { buffer.Get(), instance_buffer.Get() };
	context->IASetVertexBuffers(0, 2, vbs, strides, offsets);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);
	
	context->VSSetShader(vertex_shader.Get(), nullptr, 0);
	context->IASetInputLayout(input_layout.Get());
	context->RSSetState(rasterizer_state.Get());
	context->PSSetShader(pixel_shader.Get(), nullptr, 0);
	context->PSSetShaderResources(0, 1, shader_resource_view.GetAddressOf());
	context->PSSetSamplers(0, 1, sampler_state.GetAddressOf());
	context->OMSetDepthStencilState(depth_stencil_state.Get(), 1);


	UINT numViewports = 1;
	context->RSGetViewports(&numViewports, &viewport);

	D3D11_MAP map = D3D11_MAP_WRITE_DISCARD;
	D3D11_MAPPED_SUBRESOURCE mapped_buffer;
	hr = context->Map(instance_buffer.Get(), 0, map, 0, &mapped_buffer);
	_ASSERT_EXPR(SUCCEEDED(hr), hr_trace(hr));
	//memcpy(mappedBuffer.pData, instances, sizeof(instances));

	instances = static_cast<instance *>(mapped_buffer.pData);

	instanceCount = 0;
}

void SpriteBatch::Render(
	const float pos_x, const float pos_y, const float scale_x, const float scale_y, const float tex_pos_x, const float tex_pos_y, const float tex_scale_x, const float tex_scale_y, const float center_x, const float center_y, const float angle, const  float r, const float g, const float b, const float a, bool reverse)
{
	if (instanceCount >= MAX_INSTANCES) {
		assert(!"最大数を越えています");
		return;
	}

	if (scale_x * scale_y == 0.0f) return;
	float tw = tex_scale_x;
	float th = tex_scale_y;
	if (tw <= 0.0f || th <= 0.0f) {
		tw = (float)texture_desc.Width;
		th = (float)texture_desc.Height;
	}

	float cx = center_x;
	float cy = center_y;
	cx *= scale_x;
	cy *= scale_y;

	FLOAT c = cosf(angle * 0.01745f);
	FLOAT s = sinf(angle * 0.01745f);
	FLOAT w = 2.0f / viewport.Width;
	FLOAT h = -2.0f / viewport.Height;
	
	//NDC変換のやつ
	instances[instanceCount].ndcTransform._11 = w * scale_x * tw * c;
	instances[instanceCount].ndcTransform._21 = h * scale_x * tw * s;
	instances[instanceCount].ndcTransform._31 = 0.0f;
	instances[instanceCount].ndcTransform._41 = 0.0f;
	instances[instanceCount].ndcTransform._12 = w * scale_y * th * -s;
	instances[instanceCount].ndcTransform._22 = h * scale_y * th * c;
	instances[instanceCount].ndcTransform._32 = 0.0f;
	instances[instanceCount].ndcTransform._42 = 0.0f;
	instances[instanceCount].ndcTransform._13 = 0.0f;
	instances[instanceCount].ndcTransform._23 = 0.0f;
	instances[instanceCount].ndcTransform._33 = 1.0f;
	instances[instanceCount].ndcTransform._43 = 0.0f;
	instances[instanceCount].ndcTransform._14 = w * (-cx * c + -cy * -s + cx * 0 + pos_x) - 1.0f;
	instances[instanceCount].ndcTransform._24 = h * (-cx * s + -cy * c + cy * 0 + pos_y) + 1.0f;
	instances[instanceCount].ndcTransform._34 = 0.0f;
	instances[instanceCount].ndcTransform._44 = 1.0f;

	float tex_width = static_cast<float>(texture_desc.Width);
	float tex_height = static_cast<float>(texture_desc.Height);

	float mirror = 0.f;
	if (reverse)
		mirror = 1.f;

	instances[instanceCount].texcoordTransform = DirectX::XMFLOAT4(tex_pos_x / tex_width + mirror, tex_pos_y / tex_height, tw / tex_width, th / tex_height);
	instances[instanceCount].color = DirectX::XMFLOAT4(r,g,b,a);

	instanceCount++;
}

void SpriteBatch::Render(
	const VECTOR2F& pos,
	const VECTOR2F& scale,
	const VECTOR2F& tex_pos,
	const VECTOR2F& tex_size,
	const VECTOR2F& center,
	const float angle,
	const VECTOR4F& colour, bool reverse)
{
	Render(pos.x, pos.y, scale.x, scale.y, tex_pos.x, tex_pos.y, tex_size.x, tex_size.y, center.x, center.y, angle, colour.x, colour.y, colour.z, colour.w,reverse);
}

void SpriteBatch::Textout(std::string s,
	const float& posX, const float& posY,
	const float& scaleX, const float& scaleY,
	const float& r, const float& g, const float& b, const float& a)
{
	float tw = static_cast<float>(texture_desc.Width / 16.0);
	float th = static_cast<float>(texture_desc.Height / 16.0);
	float cursor = 0.f;

	for (const auto& c : s) {
		Render(
			VECTOR2F(posX + cursor, posY),
			VECTOR2F(scaleX, scaleY),
			VECTOR2F(tw * (c & 0x0F), th * (c >> 4)),
			VECTOR2F(tw, th),
			VECTOR2F(0.f,0.f),
			0.0f,
			VECTOR4F(r, g, b, a));

		cursor += tw * scaleX;
	}
}

void SpriteBatch::End(ID3D11DeviceContext* context)
{
	context->Unmap(instance_buffer.Get(), 0);
	context->DrawInstanced(4, instanceCount, 0, 0);
}